
Consul
  URL utils:
    http://localhost:8510/v1/catalog/services
    http://localhost:8510/v1/catalog/service/spring-cloud-consul-sample1
    http://localhost:8510/v1/health/service/spring-cloud-consul-sample1
    
spring-cloud-consul-sample1:
  bootstrap.yml
    to bind on specific address (avoid localhost):
    spring:
      consul:
        inetutils:
          preferredNetworks:
            - 192.168.1.43
      